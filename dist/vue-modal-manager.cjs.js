/*!
 * vue-modal-manager v0.0.0
 * (c) 2019-present I-Link Consulting
 */
'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

//
//
//
//
//
//
//
//
//
//
//
var script = {
  name: 'ModalManager',
  data: function data() {
    return {
      modals: this.$modals.openedModals
    };
  },
  beforeCreate: function beforeCreate() {
    this.$options.components = this.$modals.components;
  }
};

/* script */
            const __vue_script__ = script;
            
/* template */
var __vue_render__ = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.modals.length)?_c('div',{staticClass:"modal-manager"},_vm._l((_vm.modals),function(modal){return _c('div',{key:modal.id,staticClass:"modal-manager__modal"},[_c(modal.type,_vm._b({tag:"component"},'component',modal,false))],1)}),0):_vm._e()};
var __vue_staticRenderFns__ = [];

  /* style */
  const __vue_inject_styles__ = undefined;
  /* scoped */
  const __vue_scope_id__ = undefined;
  /* module identifier */
  const __vue_module_identifier__ = undefined;
  /* functional template */
  const __vue_is_functional_template__ = false;
  /* component normalizer */
  function __vue_normalize__(
    template, style, script$$1,
    scope, functional, moduleIdentifier,
    createInjector, createInjectorSSR
  ) {
    const component = (typeof script$$1 === 'function' ? script$$1.options : script$$1) || {};

    // For security concerns, we use only base name in production mode.
    component.__file = "ModalManager.vue";

    if (!component.render) {
      component.render = template.render;
      component.staticRenderFns = template.staticRenderFns;
      component._compiled = true;

      if (functional) component.functional = true;
    }

    component._scopeId = scope;

    return component
  }
  /* style inject */
  
  /* style inject SSR */
  

  
  var ModalManager = __vue_normalize__(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    undefined,
    undefined
  );

var mixin = {
  props: {
    id: Number,
    editable: Boolean,
    resolve: Function,
    reject: Function,
    data: {
      type: Object,
      default: function _default() {
        return {};
      }
    }
  },
  data: function data() {
    return {
      active: false
    };
  },
  mounted: function mounted() {
    this.active = true;
  },
  methods: {
    close: function close() {
      this.active = false; // How to wait for the start of the animation ?
      // setTimeout(() => {

      this.$modals.close(this.id); // }, 100)
    }
  },
  watch: {
    // for persistent mode
    active: function active(newValue) {
      newValue || this.$modals.close(this.id);
    }
  }
};

var ModalManager$1 =
/*#__PURE__*/
function () {
  function ModalManager$$1() {
    _classCallCheck(this, ModalManager$$1);

    this.counterModals = 0;
    this.modals = [];
  }

  _createClass(ModalManager$$1, [{
    key: "open",
    value: function open(options) {
      options.id = ++this.counterModals;
      this.modals.push(options);
    }
  }, {
    key: "close",
    value: function close(id) {
      var idx = this.modals.findIndex(function (modal) {
        return modal.id === id;
      });
      this.modals.splice(idx, 1);
    }
  }, {
    key: "install",
    value: function install(Vue, _ref) {
      var _this = this;

      var modals = _ref.modals;
      Vue.prototype.$modals = {
        components: modals,
        openedModals: this.modals,
        close: function close(id) {
          return _this.close(id);
        }
      };
      Object.keys(modals).forEach(function (modal) {
        var execModal = function execModal(options) {
          var resolve;
          var reject; // eslint-disable-next-line

          var promise = new Promise(function (resolveFromPromise, rejectFromPromise) {
            resolve = resolveFromPromise;
            reject = rejectFromPromise;
          });
          var config = Object.assign({}, options, {
            resolve: resolve,
            reject: reject,
            type: modal
          });

          _this.open(config);

          return promise;
        };

        Vue.prototype.$modals[modal] = execModal;
      });
    }
  }]);

  return ModalManager$$1;
}();
var index = new ModalManager$1();

exports.ModalManager = ModalManager;
exports.ModalMixin = mixin;
exports.default = index;
