import ModalComponent from './ModalManager.vue'
import ModalMixin from './mixin'

class ModalManager {
  constructor () {
    this.counterModals = 0
    this.modals = []
  }
  open (options) {
    options.id = ++this.counterModals
    this.modals.push(options)
  }
  close (id) {
    const idx = this.modals.findIndex(modal => modal.id === id)
    this.modals.splice(idx, 1)
  }
  install (Vue, { modals }) {
    Vue.prototype.$modals = {
      components: modals,
      openedModals: this.modals,
      close: id => this.close(id)
    }

    Object.keys(modals).forEach(modal => {
      const execModal = (options) => {
        let resolve
        let reject

        // eslint-disable-next-line
        const promise = new Promise((resolveFromPromise, rejectFromPromise) => {
          resolve = resolveFromPromise
          reject = rejectFromPromise
        })

        const config = ({
          ...options,
          resolve,
          reject,
          type: modal
        })
        this.open(config)

        return promise
      }

      Vue.prototype.$modals[modal] = execModal
    })
  }
}

export { ModalComponent as ModalManager, ModalMixin }
export default new ModalManager()
