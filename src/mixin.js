export default {
  props: {
    id: Number,
    editable: Boolean,
    resolve: Function,
    reject: Function,
    data: {
      type: Object,
      default () {
        return {}
      }
    }
  },
  data () {
    return {
      active: false
    }
  },
  mounted () {
    this.active = true
  },
  methods: {
    close () {
      this.active = false
      this.$modals.close(this.id)
    }
  },
  watch: {
    // for persistent mode
    active (newValue) {
      newValue || this.$modals.close(this.id)
    }
  }
}
